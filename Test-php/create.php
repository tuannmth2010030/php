<?php
require_once "config.php";
$title=$isbn=$available="";
$title_err=$isbn_err=$available_err="";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    //validate title
    $input_title=trim($_POST["title"]);
    if (empty($input_title)){
        $title_err="Please enter information";
    }else{
        $title=$input_title;
    }

    //isbn
    $input_isbn =trim($_POST["isbn"]);
    if (empty($input_isbn)){
        $isbn_err="Please enter information";
    }else{
        $isbn=$input_isbn;
    }
    //pub_year
//    $input_pub_year =trim($_POST["pub_year"]);
//    if (empty($input_pub_year)){
//        $pub_year_err="Please enter information";
//    }
//    elseif (!is_numeric($input_pub_year) || strlen($input_pub_year) > 6) {
//        $pub_year_err="This field must be numeric and up to 6 characters";
//    }
//    else{
//        $pub_year=$input_pub_year;
//    }
    //available
    $input_available =trim($_POST["available"]);
    if (empty($input_available)){
        $available_err="Please enter information";
    }
    elseif (!is_numeric($input_available) || strlen($input_available) > 4) {
        $available_err="This field must be numeric and up to 4 characters";
    }
    else{
        $available=$input_available;
    }
    //check input errors before inserting in database
    if (empty($title_err) && empty($isbn_err) && empty($pub_year_err) && empty($available_err)){
        //prepare an insert satement
        $sql="Insert into book (title,isbn,available) values (?,?,?)";
        if($stmt=$mysqli->prepare($sql)){
            //bind variables to the prepared statement as parameters
            $stmt->bind_param("ssi",$param_title,$param_isbn,$param_available);

            //set parameters
            $param_title=$title;
            $param_isbn=$isbn;
//            $param_pub_year=$pub_year;
            $param_available=$available;
            //attempt to execute the prepared statement
            if($stmt->execute()===true){
                header("location:dashboard.php");
                exit();
            }else{
                echo "Oops!Something went wrong.Please try again later.";
            }
        }
        $stmt->close();
    }
    $mysqli->close();
}
?>
<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mt-5">Create Record</h2>
                <p>Please fill form and submit to add Book to the database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title"class="form-control
                                <?php echo (!empty($title_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $title;?>">
                        <span class="invalid-feedback"><?php echo $title_err;?></span>
                    </div>

                    <div class="form-group">
                        <label for="">ISBN</label>
                        <input type="text" name="isbn" class="form-control
                                <?php echo (!empty($isbn_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $isbn;?>">
                        <span class="invalid-feedback"><?php echo $isbn_err;?></span>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label for="">Pub_year</label>-->
<!--                        <input type="text" name="pub_year" class="form-control-->
<!--                                --><?php //echo (!empty($pub_year_err)) ? 'is-invalid' : ''; ?><!--" value="--><?php //echo $pub_year;?><!--">-->
<!--                        <span class="invalid-feedback">--><?php //echo $pub_year_err;?><!--</span>-->
<!--                    </div>-->
                    <div class="form-group">
                        <label for="">Available</label>
                        <input type="text" name="available" class="form-control
                                <?php echo (!empty($available_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $available;?>">
                        <span class="invalid-feedback"><?php echo $available_err;?></span>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="dashboard.php" class="btn btn-secondary ml-2">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

