<?php
require_once "config.php";
$title=$isbn=$available="";
$title_err=$isbn_err=$available_err="";
if (isset($_POST["bookid"]) && !empty($_POST["bookid"])){
    $bookid=$_POST["bookid"];
    //validate title
    $input_title=trim($_POST["title"]);
    if (empty($input_title)){
        $title_err="Please enter information ";
    }else{
        $title=$input_title;
    }

    //validate isbn
    $input_isbn=trim($_POST["isbn"]);
    if (empty($input_isbn)){
        $isbn_err="Please enter information";
    }else{
        $isbn=$input_isbn;
    }
    //available
    $input_available =trim($_POST["available"]);
    if (empty($input_available)){
        $available_err="Please enter information";
    }
    elseif (!is_numeric($input_available) || strlen($input_available) > 4) {
        $available_err="This field must be numeric and up to 4 characters";
    }
    else{
        $available=$input_available;
    }
    //kiem tra du lieu dau vao
     if (empty($title_err) && empty($isbn_err) && empty($available_err)){
         $sql="update book set title=?,isbn=?,available=? where bookid=?z``";
         if ($stmt=$mysqli->prepare($sql)){
             $stmt->bind_param("ssii" ,$param_title,$param_isbn,$param_available,$param_bookid);
             $param_title=$title;
             $param_isbn=$isbn;
             $param_available=$available;
             $param_bookid=$bookid;
             if ($stmt->execute()){
                 header("location:dashboard.php");
                 exit();
             }else{
                 echo "Oop!.Something went wrong.Please try again later.";
             }
         }$stmt->close();

     }$mysqli->close();
}else{
    if (isset($_GET["bookid"]) && !empty(trim($_GET["bookid"]))){
        $bookid=trim($_GET["bookid"]);
        $sql="select * from book where bookid=?";
        if ($stmt=$mysqli->prepare($sql)){
            $stmt->bind_param("i",$param_bookid);
            $param_bookid=$bookid;
            if ($stmt->execute()){
                $result=$stmt->get_result();
                if ($result->num_rows==1){
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $title=$row["title"];
                    $isbn=$row["isbn"];
                    $available=$row["available"];
                }else{
                    header("location:error.php");
                    exit();
                }
            }else{
                echo "Opps! Something went wrong .Please try again later ";
            }
        }
        $stmt->close();
        $mysqli->close();
    }else{
        header("location:error.php");

    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="mt-5">Update recode</h2>
                <p>Please edit the input values and submit to update the Update record.</p>
                <form action="<?php echo htmlspecialchars(basename( $_SERVER['REQUEST_URI']));?>" method="post">
                    <div class="form_group">
                        <label for="">Title</label>
                        <input type="text" name="title" class="form-control
                            <?php echo (!empty($title_err))? 'is-invalid': '';?>" value="<?php echo $title;?>">
                        <span class="invalid-feedback"><?php echo $title_err;?></span>
                    </div>
                    <div class="form_group">
                        <label for="">ISBN</label>
                        <input type="text" name="isbn" class="form-control
                            <?php echo (!empty($isbn_err))? 'is-invalid': '';?>" value="<?php echo $isbn;?>">
                        <span class="invalid-feedback"><?php echo $isbn_err;?></span>
                    </div>
                    <div class="form_group">
                        <label for="">Available</label>
                        <input type="text" name="available" class="form-control
                            <?php echo (!empty($available_err))? 'is-invalid': '';?>" value="<?php echo $available;?>">
                        <span class="invalid-feedback"><?php echo $available_err;?></span>
                    </div>
                    <input type="hidden" name="bookid" value="<?php echo $bookid; ?>"/>
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="dashboard.php" class="btn btn-secondary ml-2">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>





















